import React, { Component } from 'react';
import './Todo.css';

const Checkbox = props => (
  <input type="checkbox" {...props} />
)
class Todo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      id: null,
      title: "",
      mockData: []
    }
  }

  handleChange(event) {
    event.preventDefault();

    this.setState({title: event.target.value});
  }




  onDeleteHandle() {
    let id = arguments[0];

    this.setState({
      mockData: this.state.mockData.filter(item => 
         item.id !== id)          
    });
  }

  onEditHandle(event) {
    document.getElementById(arguments[0]).setAttribute("style", "display : none");


    this.setState({
      edit: true,
      id: arguments[0],
      title: arguments[1],
    });
    
  }

  onUpdateHandle(event) {
    event.preventDefault();

    this.setState({
      mockData: this.state.mockData.map(item => {
        if (item.id === this.state.id) {
          item.title = this.state.title;
          item.edit = false;
          document.getElementById(item.id).setAttribute("style", "display : block");

        }
        return item;
      })
    });


    this.setState({
      edit: false,
      id: null
    });
  }

  onCompleteHandle() {
    let id = arguments[0];
    this.setState({
      mockData: this.state.mockData.map(item => {
        if (item.id === id) {
          item.done = !item.done;
          return item;
        }

        return item;
      })
    });
  }

  handleClick(event) {
    
    event.preventDefault();

    console.log(this.state.title);


      this.setState({
        mockData: [...this.state.mockData, {
          id: Date.now(),
          title: this.state.title,
          done: false,
          date: new Date(),
        }]
      });

     console.log(this.state)
     }

  renderEditForm(item) {

    if (this.state.id === item.id) 
    {
      return <form onSubmit={this.onUpdateHandle.bind(this)}>
           <input  type="text" className="second" value={this.state.title} onChange={this.handleChange.bind(this)}  />
           <button >Update</button>
          </form>
    }

  }

  renderForm(item) {


      return(

               
            <div className="todoListMain">
              <form id= {item.id} className="dis">
        
                
                  <span  onClick={this.onEditHandle.bind(this, item.id, item.title)}  >                 
                    {item.title }
                  </span>
                </form>
              

                <form>
                  <Checkbox className="checkbox"
                        checked={this.state.checked}
                        onChange={this.onCompleteHandle.bind(this, item.id)}
                  />
                  <button className="button" onClick={this.onDeleteHandle.bind(this, item.id)}>Delete</button>
                </form>
            </div>
      );

      
  }

  render() {
    return (
      <div>

            <form className="todo"  >
                <input  type="text" className = "first" value={this.state.title} onChange={this.handleChange.bind(this)}  />
                <button className="button" onClick={this.handleClick.bind(this)}>Add</button>
            </form>
        
            <ul className="theList" >
              {this.state.mockData.map(item => (
                <li key={item.id} className="todoItem" >
                  {this.renderForm(item)}
                  {this.renderEditForm(item)}
                </li>
              ))}
            </ul>

      </div>
    );
  }
}

export default Todo;
